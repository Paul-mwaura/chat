// Variables / Dependencies ******************************
var r = require('rethinkdb');
var assert = require('assert');

// db config settings
var dbConfig = {
	host: process.env.RDB_HOST || 'localhost',
	port: parseInt(process.env.RDB_PORT) || 28015,
	db  : process.env.RDB_DB || 'chat_app',
	tables: {
	    'messages': 'id',
	    'ipfs': 'id'
	}
};
// global connection object
var connection = {};

// DATABASE SETUP ***************************************

module.exports.setup = function() {
	// connect to db instance
	r.connect(
	// pass in config settings
	{ host: dbConfig.host, port: dbConfig.port },
	// pass in callback to be invoked upon connecting
	function (err, conn) {
	    assert.ok(err === null, err);

	    // initialize global connection object
	    connection = conn;

	    // create db if it doesn't exist
	    r.dbCreate(dbConfig.db)
	    .run(connection, function(err, result) {
			if(err) {
				console.error('database already exists');
			}
			else {
				console.log(dbConfig.db + ' database created');
			}
			// iterate over dbConfig.tables to create each one
			for (var table in dbConfig.tables) {
			// use IIFE to create closure
				(function (tbl) {
					r.db(dbConfig.db).tableCreate(tbl, {primaryKey: dbConfig.tables[table]})
					.run(connection, function(err, result) {
						if (err) {
					  		console.error(tbl + ' table already exists');
						} else {
					  		console.log(tbl + ' table created');
						}
					})
				})(table);
			}

    	});
	});
};

// MESSAGES ***********************************

module.exports.getAllMessages = function(roomAddress){
	return r.db(dbConfig.db)
	.table("messages")
	.filter({"room_id": roomAddress })
	.orderBy('time')
	.run(connection);
}

module.exports.saveMessage = function(data, roomAddress, cb){
	r.db(dbConfig.db)
	.table("messages")
	.insert({
		// "id" is auto-generated
		"text": data.text,
		"cmd": data.cmd,
		// (e.g., message, contractEvent, voteCalling, etc.)
		"time": data.time,
		"user_id": data.from,
		"room_id": roomAddress
	})
	.run(connection, cb);
}

// CONTRACTS *******************************

module.exports.saveIpfsHash = function(hash, metaData) {
	return r.db(dbConfig.db)
	.table('ipfs')
	.insert({
		'id': hash,
		'metadata': metaData
	})
	.run(connection);
}

module.exports.getDataFromIpfsHash = function(hash) {
	return r.db(dbConfig.db)
	.table('ipfs')
	.get(hash)
	.run(connection)
}
